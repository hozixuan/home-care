# Makefile with all available commands

TARGETS := $(shell grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | cut -d ':' -f 1)

.PHONY: help $(TARGETS)

help: ## Display this help message
	@echo "Available commands:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

## Global param
# set the name of virtual environment name
venv := venv
activate := . "$(venv)/bin/activate" &&

python-install-lib:
	@echo "[Development-env] Running initial setup..."
	python3 -m venv "$(venv)"
	@$(activate) pip3 install --upgrade pip
	@$(activate) pip3 install -r requirements.txt
	@echo ""
	@echo "Finished setting up the venv, begin by entering command in terminal: source venv/bin/activate"

python-install-lock-lib:
	@echo "[Development-env] Running initial setup..."
	python3 -m venv "$(venv)"
	@$(activate) pip3 install --upgrade pip
	@$(activate) pip3 install -r requirements.lock.txt
	@echo ""
	@echo "Finished setting up the venv, begin by entering command in terminal: source venv/bin/activate"

python-lib-lock-version:
	@$(activate) pip3 freeze | grep -v 0.0.0 > requirements.lock.txt

setup: | python-install-lib ## Setup venv
clean-setup: | python-install-lock-lib ## Setup venv in clean way
setup-dev: | python-install-lib ## Setup venv for development

migration: ## Migrate the model in the project
	@$(activate) python3 manage.py makemigrations
	@$(activate) python3 manage.py migrate

format: ## Format the project
	@black .

run-iot: ## Start the iot device
	@$(activate) python3 device_iot.py

run-backend: ## Run the backend server
	@$(activate) python3 manage.py runserver 0.0.0.0:8000 --noreload

createsuperuser: ## Create superuser for the admin site
	@$(activate) python3 manage.py createsuperuser

collectstatic: ## Collect the static files and put into the folder
	@$(activate) python3 manage.py collectstatic

