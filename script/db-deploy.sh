#!/bin/bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
WORK_DIR="${SCRIPT_DIR}/.."

# step2. down the server
docker-compose \
    -f "${WORK_DIR}"/db.dc.yml \
    down

# step4. up the server
docker-compose \
    -f "${WORK_DIR}"/db.dc.yml \
    up -d
