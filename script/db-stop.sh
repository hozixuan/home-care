#!/bin/bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
WORK_DIR="${SCRIPT_DIR}/.."

# stop the server
docker-compose \
    -f "${WORK_DIR}"/db.dc.yml \
    down
