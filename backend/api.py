from ninja import NinjaAPI


def get_ninja_api():
    api = NinjaAPI(
        openapi_extra={},
        title="Backend API",
        version="1.0.0",
        description="This is API",
    )
    # api = NinjaAPI()

    api.add_router("/", "app_event.api.router")
    return api
