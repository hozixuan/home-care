# middlewares.py


class CustomCorsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Process the request
        response = self.get_response(request)

        try:
            response["Access-Control-Allow-Origin"] = request.headers["Origin"]
        except:
            pass

        return response
