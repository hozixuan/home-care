#!/bin/bash

set -e

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
WORK_DIR="${SCRIPT_DIR}"
# go into current dir to make sure everything work as per intended
cd "$WORK_DIR"

EC2_IP="$(head -n1 a-rpi-ip.txt)"
PROJ_RPATH=$(realpath -s --relative-to="$HOME" "$(pwd)")/
KEY_LOC="$(tail -n1 a-rpi-ip.txt)"

echo "PROJ_RPATH $PROJ_RPATH"

echo "Create folder..."
ssh "${EC2_IP}" "mkdir -p ~/${PROJ_RPATH}"

echo "Syncing..."
# Rsync ignore gitignore
# https://stackoverflow.com/questions/13713101/rsync-exclude-according-to-gitignore-hgignore-svnignore-like-filter-c
rsync -arvP --update \
    --include='**.gitignore' \
    --filter="dir-merge,- .gitignore" \
    --exclude-from="a-rsync_exclude.txt" \
    "${HOME}/${PROJ_RPATH}" \
    "${EC2_IP}":~/${PROJ_RPATH}
# --exclude='/.git' \

echo "Job dones!"
