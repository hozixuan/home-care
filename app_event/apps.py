from django.apps import AppConfig


class AppEventConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "app_event"

    # def ready(self):
    #     from .models import AppEvent
    #     from django.db.models import F

    #     datas = AppEvent.objects.all()

    #     for data in datas:
    #         print(data)
