from ninja import Router
import psutil
from .models import AppEvent
from typing import List
from pydantic import BaseModel

router = Router()


@router.get("/")
def device_status(request):
    memory = psutil.virtual_memory()
    disk = psutil.disk_usage("/")

    temp = 0
    try:
        temp = psutil.sensors_temperatures()["cpu_thermal"][0].current
    except:
        pass

    system_info_data = {
        "cpu_percent": psutil.cpu_percent(1),
        "cpu_count": psutil.cpu_count(),
        "cpu_freq": psutil.cpu_freq(),
        "cpu_mem_total": memory.total,
        "cpu_mem_avail": memory.available,
        "cpu_mem_used": memory.used,
        "cpu_mem_free": memory.free,
        "disk_usage_total": disk.total,
        "disk_usage_used": disk.used,
        "disk_usage_free": disk.free,
        "disk_usage_percent": disk.percent,
        "sensor_temperatures": temp,
    }

    return system_info_data


### Saving Data ###

import datetime


class AppEventIn(BaseModel):
    """The happened app event"""

    ts: datetime.datetime
    obj_id: int
    xmin: int
    ymin: int
    xmax: int
    ymax: int
    classes: str
    image_id: str


class DataIn(BaseModel):
    items: List[AppEventIn]


@router.post("/app_event")
def save_app_event(request, datas: DataIn):
    print(datas)
    for data in datas.items:
        app_event = AppEvent(
            ts=data.ts,
            obj_id=data.obj_id,
            loc_xmin=data.xmin,
            loc_ymin=data.ymin,
            loc_xmax=data.xmax,
            loc_ymax=data.ymax,
            classes=data.classes,
            image_id=data.image_id,
        )
        app_event.save()
        print(app_event)

    return {"code": 200, "message": "success"}


### Image URL
import os
from django.http import FileResponse
from django.conf import settings


@router.get("/images/{image_id}")
def get_image(request, image_id: str):
    image_path = os.path.join(settings.MEDIA_ROOT, f"{image_id}.jpg")
    try:
        print(f"Image found!{image_path=}")
        return FileResponse(
            open(image_path, "rb"), content_type="image/jpeg"
        )  # Adjust content type based on your image type
    except Exception as err:
        print(f"Fail to open the image! Reason={err}")
        return {"code": 404, "message": "Image not found"}


### API calculation ###
from django.utils import timezone
from django.db.models import Count, Max, Q, Min
from django.db.models.functions import TruncHour, TruncDate


# Define a function to count distinct image_ids per hour
def count_distinct_image_ids_per_day():
    # Use Django ORM to perform aggregation
    distinct_image_counts = (
        AppEvent.objects.annotate(date=TruncDate("ts"))  # Truncate timestamp to hour
        .values("date")  # Group by hour
        .annotate(
            num_events=Count("image_id", distinct=True)
        )  # Count distinct image_ids per hour
        .order_by("-date")  # Optionally, order by hour
    )

    results = list(distinct_image_counts)

    return results


@router.get("/event_per_day")
def get_event_per_day(request):
    results = count_distinct_image_ids_per_day()

    return {"items": results}


# Define a function to count distinct image_ids per hour
def count_distinct_image_ids_per_hour():
    # Calculate datetime for 24 hours ago from current time
    twenty_four_hours_ago = timezone.now() - timezone.timedelta(hours=24)

    # Use Django ORM to perform aggregation
    distinct_image_counts = (
        AppEvent.objects.filter(
            ts__gte=twenty_four_hours_ago
        )  # Filter records with timestamp >= 24 hours ago
        .annotate(hour=TruncHour("ts"))  # Truncate timestamp to hour
        .values("hour")  # Group by hour
        .annotate(
            num_events=Count("image_id", distinct=True)
        )  # Count distinct image_ids per hour
        .order_by("hour")  # Optionally, order by hour
    )

    results = list(distinct_image_counts)

    return results


def fill_in_missing_time(datas):
    ## fill in missing hour
    # Convert 'hour' strings to datetime objects
    for item in datas:
        item["hour"] = datetime.datetime.fromisoformat(
            str(item["hour"])[:-6]
        )  # Remove timezone offset for parsing

    # Determine the current hour (rounded down to the nearest hour)
    current_hour = datetime.datetime.now().replace(minute=0, second=0, microsecond=0)

    # Generate a list of hourly timestamps for the past 24 hours
    hourly_range = [current_hour - datetime.timedelta(hours=i) for i in range(24)]

    # Create a mapping of existing data hours to their respective values
    existing_data_map = {item["hour"]: item["num_events"] for item in datas}

    # Create a list of data points for the past 24 hours, filling in missing hours with 0
    complete_data = [
        {"hour": hour, "num_events": existing_data_map.get(hour, 0)}
        for hour in hourly_range
    ]

    # Sort complete_data by 'hour' in ascending order (if needed)
    complete_data.sort(key=lambda x: x["hour"])

    return complete_data


@router.get("/event_per_hour")
def get_event_per_hour(request):
    results_a = count_distinct_image_ids_per_hour()
    results = fill_in_missing_time(results_a)

    return {"items": results}


# Define a function to count occurrences of "car" or "human" in the classes field
def count_classes():
    # Use Django ORM to filter and count occurrences
    class_counts = AppEvent.objects.values("classes").annotate(count=Count("classes"))

    return list(class_counts)


@router.get("/event_type_count")
def get_event_type_count(request):
    class_counts = count_classes()

    results = []
    # Print the results
    for class_item in class_counts:
        results.append(
            {
                "class": class_item["classes"],
                "count": class_item["count"],
            }
        )

    return {"items": results}


from django.forms.models import model_to_dict


@router.get("/latest_car_event")
def get_latest_car_event(request):

    try:
        latest_event = AppEvent.objects.filter(classes__icontains="car").latest("ts")
        results = model_to_dict(latest_event)
    except Exception as err:
        print(f"Error to get data. Reason={err}")
        results = {}
    return results


@router.get("/latest_person_event")
def get_latest_person_event(request):
    try:
        latest_event = AppEvent.objects.filter(classes__icontains="person").latest("ts")
        results = model_to_dict(latest_event)
    except Exception as err:
        print(f"Error to get data. Reason={err}")
        results = {}
    return results
