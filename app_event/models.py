from django.db import models

# Create your models here.


class AppEvent(models.Model):
    """The happened app event"""

    ts = models.DateTimeField()
    obj_id = models.IntegerField()
    loc_xmin = models.IntegerField()
    loc_ymin = models.IntegerField()
    loc_xmax = models.IntegerField()
    loc_ymax = models.IntegerField()
    classes = models.CharField(max_length=100)
    image_id = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.ts}, {self.image_id}, {self.classes}"
