def _get_logger():
    import logging

    # create logger
    logger = logging.getLogger("App")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter(
        "%(asctime)s.%(msecs)03d[%(levelname)s]:%(message)s", "%Y-%m-%d_%H:%M:%S"
    )

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
    return logger


logger = _get_logger()
