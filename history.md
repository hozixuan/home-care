python3 -m venv venv
pip3 install --upgrade pip
pip3 install torch torchvision
pip3 install opencv-python
pip3 install numpy --upgrade
pip3 install ultralytics

# cpu installation
pip3 install torch==2.3.0 torchvision==0.18.0 --index-url https://download.pytorch.org/whl/cpu

## IMage generate size

```sh
python3 -c 'perimg_size_kb=250;snap_interval_sec=5;size_generate_per_day=(60/snap_interval_sec)*60*24*perimg_size_kb;print(f"It will generate {round(size_generate_per_day/1024/1024, 2)} GB, with snap interval {snap_interval_sec}s, Each img size {perimg_size_kb}KB")'
```

- [Do raspi need fan cooling? A: no it will just throttle cpu if too hot](https://forums.raspberrypi.com/viewtopic.php?t=268547)
- [Setup raspi with postgres](https://pimylifeup.com/raspberry-pi-postgresql/)
- [Setup raspi with postgres with remote enable](https://dataslinger.medium.com/setting-up-remote-postgresql-on-raspberry-pi-f9058b415768)
