from ultralytics import YOLO
import cv2
import datetime
import pytz
import requests

from app_logger import logger

VIDEO_STREAM = "rtsp://kbkl:kbklseanne@192.168.1.100:554/channel=4"
# VIDEO_STREAM = "rtsp://kbkl:kbklseanne@192.168.1.100:554/channel=7"
# VIDEO_STREAM = "rtsp://kbkl:kbklseanne@192.168.1.100:554/channel=6"
BACKEND_SERVER_URL = "http://0.0.0.0:8000/api/app_event"
AWAIT_INTERVAL_SEC = 5 * 60


model = YOLO("model/yolov8n.pt")
results = model(VIDEO_STREAM, stream=True, verbose=False)
# Define the start time
def get_time_now():
    return datetime.datetime.now(pytz.timezone("Asia/Singapore"))


start_time = get_time_now()

# Define the time interval (5 seconds in this case)
time_interval = datetime.timedelta(seconds=AWAIT_INTERVAL_SEC)
logger.info(
    f"Program initialized complete, wait for {AWAIT_INTERVAL_SEC}s to perform action"
)

try:
    while True:

        for idx, res in enumerate(results):
            # Check if the time interval has passed
            current_time = get_time_now()
            elapsed_time = current_time - start_time

            if elapsed_time < time_interval:
                continue
            # Execute the process when the time interval has passed
            logger.info(f"Time interval passed after {idx} iterations")

            # Reset the start time for the next interval
            start_time = current_time
            image_id = start_time.strftime("%Y%m%d%H%M%S")

            # Perform result retrieving
            boxes = res.boxes.data
            names = res.names  ## Grab the dictionary

            ## Loop the result
            obj_id = 0
            det_objs = []

            for box in boxes:
                xmin: int = int(box[0])
                ymin: int = int(box[1])
                xmax: int = int(box[2])
                ymax: int = int(box[3])
                obj_conf = float(box[4])
                label_id = int(box[5])
                obj_result = names[label_id]

                if obj_result in ["person", "car"]:

                    # Assign ID for each detected object of interest
                    obj_id += 1

                    # For better obj_confident output
                    obj_conf = round(obj_conf * 100, 2)

                    det_objs.append(
                        {
                            "ts": current_time.isoformat(),
                            "obj_id": obj_id,
                            "xmin": xmin,
                            "ymin": ymin,
                            "xmax": xmax,
                            "ymax": ymax,
                            "classes": obj_result,
                            "image_id": image_id,
                        }
                    )

            datas = {"items": det_objs}

            # image output
            ori_im = res.orig_img
            if len(det_objs) > 0:
                logger.info(f"{datas}")
                draw_im = ori_im.copy()
                # Draw a rectangle outline
                for det_obj in det_objs:
                    xmin = det_obj["xmin"]
                    xmax = det_obj["xmax"]
                    ymin = det_obj["ymin"]
                    ymax = det_obj["ymax"]
                    start_point = (xmin, ymin)
                    end_point = (xmax, ymax)
                    cv2.rectangle(draw_im, start_point, end_point, (0, 0, 255), 2)
                cv2.imwrite(f"media/{image_id}.jpg", draw_im)

                try:
                    resp = requests.post(url=BACKEND_SERVER_URL, json=datas)
                    print("resp ", resp.json())
                except Exception as err:
                    print(f"Fail to post to endpoint {BACKEND_SERVER_URL} reason {err}")

            ## Show the image
            cv2.imwrite("media/live_image.jpg", ori_im)

            # ## Ploting result
            # ## Ref: https://docs.ultralytics.com/modes/predict/#plotting-results
            # res_plotted = res.plot()
            # img_save_name = "media/{}.png".format(idx)
            # cv2.imwrite(img_save_name, res_plotted)  # Save down the image

            # ## Show the image
            # # cv2.imshow(img_save_name, res_plotted)
            # cv2.imshow("image", res_plotted)
            # key = cv2.waitKey(10)
            # if key == ord("q"):
            #     break
            # # cv2.destroyWindow(img_save_name)


except Exception as err:
    logger.info(f"Error with {err}")
finally:
    cv2.destroyAllWindows()
logger.info("End of the program----")
