# Home Care

A companion help you to take care your home!

## Getting start

1. Setup the project with `make setup`
2. Run the IoT Device with `make run-iot`
3. Start the backend server `make run-backend`
