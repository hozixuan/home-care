#!/bin/bash

set -e

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
WORK_DIR="${SCRIPT_DIR}"
# go into current dir to make sure everything work as per intended
cd "$WORK_DIR"

EC2_IP="$(head -n1 a-rpi-ip.txt)"
PROJ_RPATH=$(realpath -s --relative-to="$HOME" "$(pwd)")/
KEY_LOC="$(tail -n1 a-rpi-ip.txt)"

ssh "${EC2_IP}"
